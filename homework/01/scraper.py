import requests
import datetime
from bs4 import BeautifulSoup
import time

SNOOZE_TIMER = 1
WEIRD_SPACE = '\xa0'
BASE_URL = 'https://www.psp.cz/sqw/hlasy.sqw?g='
FIRST_SESSION_NO = 67018
LAST_SESSION_NO = 74002
SESSION_FILE = 'sessions.csv'
DEPUTY_VOTES_FILE = 'deputy_votes.csv'
HEADERS_SESSIONS = 'vote_id,topic,date\n'
HEADERS_VOTES = 'vote_id,name,party,vote\n'
MONTHS = ['ledna', 'února', 'března',
          'dubna', 'května', 'června',
          'července', 'srpna', 'září',
          'října', 'listopadu', 'prosince']


def parse_date_time(date) -> datetime.datetime:
    parsed_date = date.split('.')

    return datetime.datetime(int(parsed_date[1].split(WEIRD_SPACE)[2]),
                             MONTHS.index(parsed_date[1].split(WEIRD_SPACE)[1]) + 1,
                             int(parsed_date[0]))


def parse(page_content, session_id) -> None:
    h1 = page_content.findAll('h1')

    topic = str(h1[0]).split('<br/>')[1].split('<')[0]
    date = parse_date_time(str(h1[0]).split(',')[2])

    with open(SESSION_FILE, 'w') as file:
        file.write(str(session_id) + ',' + topic.replace(',', '|') + ',' + str(date) + '\n')

    parties = page_content.findAll('h2', {'class': 'section-title center'})
    parties = parties[1:-1]

    for party in parties:
        party_name = str(party).split('>')[2].split(' ')[0]
        voters = party.next_sibling

        with open(DEPUTY_VOTES_FILE, 'w') as file:
            for voter in voters:
                file.write('{0},{1},{2},{3}\n'.format(str(session_id),
                                                      str(voter.find('a')).split('>')[1].split('<')[0],
                                                      party_name,
                                                      str(voter).split('>')[2].split('<')[0]))


with open(SESSION_FILE, 'w') as file:
    file.write(HEADERS_SESSIONS)
with open(DEPUTY_VOTES_FILE, 'w') as file:
    file.write(HEADERS_VOTES)

for session_id in range(FIRST_SESSION_NO, LAST_SESSION_NO):
    page = requests.get(BASE_URL + str(session_id))
    soup = BeautifulSoup(page.content, 'html.parser')
    if soup.head.title is not None:
        parse(soup, session_id)

    time.sleep(SNOOZE_TIMER)
